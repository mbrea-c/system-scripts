    #!/bin/sh
    # Open a command in current desktop
    
    usage() {
    	echo "No command specified."
    }
    
    if [ $# = 0 ]; then
    	usage
    	exit 1
    fi
    
    # Gets current desktop name. Probably will break if some of your desktop names
    # include the character 'O' or you have more than one monitor, I haven't 
    # tested thoroughly
    DESKTOP="$(bspc wm --get-status | tr ":" "\n" | grep O | tr -d "O")"
    
    # Creates a single-shot rule that opens next application in $DESKTOP
    bspc rule -a \* -o desktop=$DESKTOP && $@
