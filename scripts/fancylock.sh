#!/bin/bash

B='#00000000'  # blank
K='#00000077'  # dark
R='#ffffffff'  # default ring color - white
C='#ffffff22'  # clear ish - Inside while veryfying
D='#e27c3fff'  # default
T='#ee00eeee'  # text
W='#880000ff'  # wrong
V='#0000bbff'  # verifying

# Take screenshot
scrot -o /tmp/scrlck.png

effect=(-scale 10% -scale 1000%)

# Blur screenshot and superpose lock icon
convert /tmp/scrlck.png ${effect[@]} -modulate 80,50,100 -gravity center ~/.config/fancylock/lock-white.png -composite /tmp/scrlck.png
# convert /tmp/scrlck.png -paint 1 -swirl 360 -gravity center ~/.config/fancylock/lock.png -composite /tmp/scrlck.png

# Lock screen
i3lock -e -f -i /tmp/scrlck.png \
--insidevercolor=$B   \
--ringvercolor=$V     \
\
--insidewrongcolor=$B \
--ringwrongcolor=$W   \
\
--keyhlcolor=$K       \
--insidecolor=$B      \
--ringcolor=$R        \
--linecolor=$B        \
--separatorcolor=$B   \
\
--veriftext=""        \
--wrongtext=""        \
--noinputtext=""      \
\
--ring-width=8
