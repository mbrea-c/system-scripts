#!/bin/sh

# Shows CPU fan speeds corresponding to each available level through acpi

cleanup () {
	echo "level auto" > /proc/acpi/ibm/fan && echo "level auto set"
	echo "Leaving with..."
	cat /proc/acpi/ibm/fan
	exit 0
}

trap cleanup INT

for i in 0 1 2 3 4 5 6 7; do
    echo "level $i" > /proc/acpi/ibm/fan
    echo "level $i..."
    sleep 15
    cat /proc/acpi/ibm/fan | egrep "^speed"
    echo
done

cleanup
