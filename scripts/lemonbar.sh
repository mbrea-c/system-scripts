#!/usr/bin/sh

# Define global variables
SEP="  "
PAD=" "
BAR_SEGMENTS="20"
BACKGROUND="$(xrdb -query | awk '/*.background/ { print $2 }')"
FOREGROUND="$(xrdb -query | awk '/*.foreground/ { print $2 }')"
PID_FILE="/tmp/lemonbar_tmp_pid"
for i in $(seq 0 15); do
	eval COLOR$i="\$(xrdb -query | awk '/*.color$i:/ { print \$2 }')"
done

# print_with_color FOREGROUND BACKGROUND TEXT
print_with_color() {
	echo -n "%{F$1}%{B$2}$3%{F$FOREGROUND}%{B$BACKGROUND}"
}

#Define the battery
battery() {
	case "$(cat /sys/class/power_supply/BAT0/status)" in
		Charging)
			BATSTATUS="adp"
			;;
		Discharging)
			BATSTATUS="bat"
			;;
		*)
			BATSTATUS="idl"
			;;
	esac

        BATPERC=$(cat /sys/class/power_supply/BAT0/capacity)
        echo "$BATSTATUS $BATPERC%"
}

time() {
	echo "$(date "+%H:%M")"
}

bspwm() {
	for desktop in $(bspc query -D --names); do
		case "$(bspc wm -g | tr : '\n' | grep ^\[fFoO]$desktop\$ | cut -c1)" in
			o)
				echo -n "$(print_with_color $FOREGROUND $BACKGROUND $desktop )"
				;;
			F)
				echo -n "$(print_with_color $BACKGROUND $COLOR8 " $desktop ")"
				;;
			O)
				echo -n "$(print_with_color $FOREGROUND $COLOR8 " $desktop ")"
				;;
			f)
				echo -n "$(print_with_color $COLOR8 $BACKGROUND $desktop)"
				;;
			*)
				echo -n "$(print_with_color $COLOR8 $BACKGROUND $desktop)"
				;;
		esac
		echo -n " "
	done
}

active_window() {
	echo "$(xdotool getwindowname $(xdotool getactivewindow) | cut -c -80)"
}


# print_bar PERCENTAGE SEGMENTS LABEL
print_bar() {
	percentage=$1
	segments=$2
	bars=$(( ($percentage*$segments)/100 ))

	echo -n "$3 "
	for i in $(seq 0 $segments); do
		if [ $(($i < $bars)) = 1 ]; then
			print_with_color $FOREGROUND $BACKGROUND '—'
		elif [ $(($i == $bars)) = 1 ]; then
			print_with_color $FOREGROUND $BACKGROUND '|'
		else
			print_with_color $COLOR8 $BACKGROUND '—'
		fi
	done
}

brightness_bar() {
	backlight=$(xbacklight -get | cut -d. -f1)
	print_bar $backlight $BAR_SEGMENTS scr
}

volume_bar() {
	volume=$(pamixer --get-volume)
	print_bar $volume $BAR_SEGMENTS vol
}

update() {
	while true; do
		trap : USR1
		# Print left
		echo -n "%{l}$PAD$(bspwm)"
		# Print center
		echo -n "%{c}$(active_window)"
		# Print right
		echo "%{r}$(brightness_bar)$SEP$(volume_bar)$SEP$(battery)$SEP$(print_with_color $COLOR0 $COLOR8 " $(time) ")$PAD"
		trap update USR1

		# Kill previous sleep and start new
		kill $!
		sleep 5 & wait $!;
	done
}

echo $$ >$PID_FILE
update



