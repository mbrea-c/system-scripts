#!/bin/env python3

import sys
import os

from pdfrw import PdfReader, PdfWriter, PageMerge

inpdocs = sys.argv[1:]
outfn = 'merged.' + os.path.basename(inpdocs[0])
opages = []

for inpfn in inpdocs:
    opages += PdfReader(inpfn).pages

writer = PdfWriter(outfn)
writer.addpages(opages)
writer.write()
