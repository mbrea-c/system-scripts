#!/bin/sh

if [ $# != 1 -a $# != 2 ]; then
	echo "Too many arguments"
	exit 1
fi

TARGET=$1

if [ "$2" = "-b" ]; then
	set -- -shave 1x1 -bordercolor black -border 1
else
	set --
fi



TEMPDIR=$(mktemp -d)
convert -density 200 -scale x800 $TARGET $@ $TEMPDIR/page.png
n_lines=$(ls -1 $TEMPDIR | wc -l)

# Rename files so that I can do a numeric sort later 
for i in $(seq 1 "$n_lines"); do
	mv $TEMPDIR/page-$(($i-1)).png $TEMPDIR/$(($i-1))-page.png 
done

# Allow user to skip certain pages
for i in $(seq 1 "$n_lines"); do
	printf "Skip page $i?[Y/n]:"
	read skip
	if [ "$skip" = "y" -o "$skip" = "Y" ]; then
		echo "Skipping $TEMPDIR/$(($i-1))-page.png"
		rm $TEMPDIR/$(($i-1))-page.png
	fi
done

# Had to do some hacks here to make this work on unix shell
# This next part is a mystery
counter=1
suffix=$(echo 0 | awk '{printf "%04d\n", $0;}')
for page in $(ls -1 $TEMPDIR | sort -n); do
	case $counter in
		1)
			page1=$TEMPDIR/$page
			;;
		2)
			page2=$TEMPDIR/$page
			;;
		3)
			page3=$TEMPDIR/$page
			counter=0
			suffix=$(echo "$suffix + 1" | bc -l | awk '{printf "%04d\n", $0;}')
			;;
	esac
	counter=$(($counter+1))
	if [ $counter = 1 ]; then
		convert $page1 $page2 $page3 -gravity center -append "$TEMPDIR/out_$suffix.png"
	fi
done
convert $page1 -fill white -draw 'color 0,0 reset' $TEMPDIR/white.jpg
white="$TEMPDIR/white.jpg"
case $counter in
	1)
		;;
	2)
		suffix=$(echo "$suffix + 1" | bc -l | awk '{printf "%04d\n", $0;}')
		convert $page1 $white $white -gravity center -append "$TEMPDIR/out_$suffix.png"
		;;
	3)
		suffix=$(echo "$suffix + 1" | bc -l | awk '{printf "%04d\n", $0;}')
		convert $page1 $page2 $white -gravity center -append "$TEMPDIR/out_$suffix.png"
		;;
esac
# This is a workaround for a bug in ImageMagick that has been around
# since 2013 (that is known of). Leave as is
convert $TEMPDIR/out_* -resize 1190x1684 -gravity center -background white -extent 1190x1684 -units pixelsperinch -density 72 -page A2 trimmed-$TARGET \
	&& convert trimmed-$TARGET -page A4 trimmed-$TARGET
rm -r $TEMPDIR
exit 0

