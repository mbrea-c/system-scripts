#!/bin/sh

TASKLIST_FILE=$HOME/TODO.txt

cat $TASKLIST_FILE | groff -ms -Tascii > /tmp/tasklist.tmpdoc

# Open compiled doc in a new floating terminal
/bin/env bspwm-open-floating urxvt -e less -R /tmp/tasklist.tmpdoc
